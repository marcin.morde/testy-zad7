#include <cassert>
#include "../tri_list.h"

int main() {
    auto a = [](int x) { return x + 1; };
    auto b = [](int x) { return x * 2; };
    assert(compose<int>(a, b)(1) == 3);
    assert(compose<int>(b, a)(1) == 4);
}
