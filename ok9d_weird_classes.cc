#include "../tri_list.h"
#include <cassert>
#include <cstdio>
#include <iostream>
#include <set>

using std::set;

class boomboomboom : public std::exception {
	const char *what() const throw() {
		return "69";
	}
};

class No_constructors {
private:
	int dupa;

public:
	No_constructors(int x) = delete;
	No_constructors(const No_constructors &x) = delete;
	No_constructors(No_constructors &&x) = default;
	No_constructors &operator=(const No_constructors &x) = default;
	No_constructors &operator=(No_constructors &&x) = default;

	int operator()() {
		return dupa;
	}
};

int main() {
	tri_list<int, double, No_constructors> nocon;
    nocon.push_back<int>(69);
    nocon.reset<No_constructors>();
    nocon.begin();
    nocon.end();
    [[maybe_unused]] auto xd = identity<No_constructors>;
}
