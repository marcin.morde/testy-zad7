#include "../tri_list.h"
#include <cassert>
#include <set>

using std::set;

int main() {
	tri_list<int, set<int>, double> l;
	l.push_back<set<int>>(set<int>());
	l.modify_only<set<int>>(identity<set<int>>);
	*l.range_over<set<int>>().begin();

	// Sprawdzanie, czy reset nie modyfikuje zawartości setow

	set<int> secik{41, 69};
	l.push_back<set<int>>(secik);

	l.modify_only<set<int>>([](set<int> s) {
		s.clear();
		return s;
	});

	for (auto s : l.range_over<set<int>>()) {
		assert(s.empty());
	}

	l.reset<set<int>>();

	size_t sum_of_sizes = 0;
	for (auto s : l.range_over<set<int>>()) {
		sum_of_sizes += s.size();
	}

	assert(sum_of_sizes == 2);

	///////////////////////////////////////////////////////

	tri_list<set<double>, set<int>, set<long double>> l2;
	l2.push_back<set<int>>(set<int>());
	l2.push_back<set<double>>(set<double>{3.1415, 41.69});
	l2.modify_only<set<long double>>([](set<long double> s) {
		s.insert(0.000001);
		return s;
	});

	l2.push_back<set<long double>>(
	    set<long double>{69, 'E', 0x45, 0105, 69.0000000001});

	for (auto s : l2.range_over<set<long double>>()) {
		assert(s.size() == 3);
		break;
	}
}
