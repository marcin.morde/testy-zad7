#include "../tri_list.h"
#include <cassert>
#include <cstdio>
#include <iostream>
#include <set>

using std::set;

class boomboomboom : public std::exception {
	const char *what() const throw() {
		return "69";
	}
};

class No_assignment {
private:
	int dupa;

public:
	No_assignment(int x)
	: dupa(x) {}
	No_assignment(const No_assignment &x) = default;
	No_assignment(No_assignment &&x) = default;
	No_assignment &operator=(No_assignment x) = delete;
	No_assignment &operator=(const No_assignment &x) = delete;
	No_assignment &operator=(No_assignment &&x) = delete;

	int operator()() {
		return dupa;
	}
};

int main() {
	tri_list<int, double, No_assignment> noas;
	noas.push_back<int>(69);
	noas.push_back<No_assignment>(No_assignment(69));
	noas.modify_only<No_assignment>([](No_assignment no) {
		No_assignment new_as(no() + 69);
		return new_as;
	});
	noas.begin();
	noas.end();
	*noas.range_over<No_assignment>().begin();
	for (auto no : noas.range_over<No_assignment>()) {
		assert(no() == 138);
	}
	noas.reset<No_assignment>();
}
